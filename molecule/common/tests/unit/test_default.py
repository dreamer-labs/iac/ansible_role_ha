import itertools
import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_vip_address(ansible_vars):
    vip_interface = ansible_vars["ha_clusters"][0]["resources"][0]["options"][
        "nic"
    ]
    vip_address = ansible_vars["ha_clusters"][0]["resources"][0]["options"][
        "ip"
    ]

    host_interface_addresses = []

    for hostname in testinfra_hosts:
        host = testinfra.utils.ansible_runner.AnsibleRunner(
            os.environ["MOLECULE_INVENTORY_FILE"]
        ).get_host(hostname)

        host_interface_addresses.append(
            host.interface(vip_interface).addresses
        )

    assert vip_address in list(
        itertools.chain.from_iterable(host_interface_addresses)
    )
