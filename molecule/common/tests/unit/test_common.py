import os
import pytest

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.mark.parametrize("pkg", ["pcs"])
def test_pkg(host, pkg):
    package = host.package(pkg)

    assert package.is_installed


def test_pcs_running_and_enabled(host):
    pcs = host.service("pcsd")
    assert pcs.is_running
    assert pcs.is_enabled
