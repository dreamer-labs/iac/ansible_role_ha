import os
import time

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def current_vip_host(vip_interface, vip_address):

    host_interface_addresses = []

    for hostname in testinfra_hosts:
        host = testinfra.utils.ansible_runner.AnsibleRunner(
            os.environ["MOLECULE_INVENTORY_FILE"]
        ).get_host(hostname)

        host_interface_addresses.append(
            {"host": hostname, "ip": host.interface(vip_interface).addresses}
        )

    for entry in host_interface_addresses:
        if vip_address in entry["ip"]:
            host = testinfra.utils.ansible_runner.AnsibleRunner(
                os.environ["MOLECULE_INVENTORY_FILE"]
            ).get_host(entry["host"])

            return host


def test_vip_switches_over(vip_address, vip_interface):

    old_vip = (
        current_vip_host(vip_interface, vip_address).run("hostname").stdout
    )
    current_vip_host(vip_interface, vip_address).run(
        'nohup sudo -b bash -c "sleep 5; reboot" &>/dev/null'
    )
    time.sleep(60)

    new_vip = (
        current_vip_host(vip_interface, vip_address).run("hostname").stdout
    )

    assert old_vip != new_vip


def test_httpd_switches_over(vip_address, vip_interface):

    current_vip_host(vip_interface, vip_address).run(
        'nohup sudo -b bash -c "sleep 5; reboot" &>/dev/null'
    )
    time.sleep(60)

    assert (
        current_vip_host(vip_interface, vip_address)
        .service("httpd")
        .is_running
    )
