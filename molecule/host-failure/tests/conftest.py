import os
import pytest
import yaml


@pytest.fixture
def ansible_vars():
    pth = os.path.realpath(os.path.join(__file__, "..", "..", "converge.yml"))
    with open(pth) as ymlfile:
        ansible_vars = yaml.safe_load(ymlfile)[0]["roles"][0]["vars"]

    return ansible_vars


@pytest.fixture
def vip_address(ansible_vars):
    vip_address = ansible_vars["ha_clusters"][0]["resources"][0]["options"][
        "ip"
    ]

    return vip_address


@pytest.fixture
def vip_interface(ansible_vars):
    vip_interface = ansible_vars[
        "ha_clusters"][0]["resources"][0]["options"]["nic"]

    return vip_interface
