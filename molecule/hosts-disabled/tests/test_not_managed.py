import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_hosts_file_default(host):
    f = host.file('/etc/hosts')

    hostname = host.ansible.get_variables()['inventory_hostname'].replace(
        '_', '-')

    assert f.contains(f'127.0.0.1\\s{hostname}')


def test_cloud_init_config_default(host):
    f = host.file('/etc/cloud/cloud.cfg')

    assert f.contains('update_etc_hosts')


def test_user_does_not_exits(host):
    assert not host.user('hacluster').exists
