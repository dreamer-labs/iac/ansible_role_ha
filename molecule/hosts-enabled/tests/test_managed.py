import os
import re
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_hosts_file_default(host):
    f = host.file('/etc/hosts')

    hostname = host.ansible.get_variables()['inventory_hostname'].replace(
        '_', '-')

    assert not f.contains(f'127.0.0.1\\s{hostname}')


def test_hosts_file_host_entries_added(host):
    f = host.file('/etc/hosts')

    pattern = '[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}'

    for itter_host in testinfra_hosts:
        hostname = itter_host.replace('_', '-')
        assert re.search(f'{pattern}\\s{hostname}', f.content_string)


def test_cloud_init_config_default(host):
    f = host.file('/etc/cloud/cloud.cfg')

    assert not f.contains('update_etc_hosts')
