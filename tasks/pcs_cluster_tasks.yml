---
- name: import cluster facts
  import_tasks: pcs_cluster_facts.yml

- name: Setup the cluster
  command: "pcs cluster setup --name {{ cluster.name }} {{ cluster.nodes | join(' ') }}"
  when:
    - '(cluster_info_name is not defined) or ("unable to get cib" in cluster_info.stderr)'

- name: Start the cluster
  command: "pcs cluster start --all"
  when:
    - '"unable to get cib" in cluster_info.stderr'

- name: Ensure the cluster starts on boot
  command: "pcs cluster enable --all"
  when:
    - '"unable to get cib" in cluster_info.stderr'

- name: Set Properties for the cluster
  command: "pcs property set {{ item.name }}={{ item.value }}"
  loop: "{{ cluster.properties }}"
  when: >
    (cluster_info_properties is not defined) or
    ("unable to get cib" in cluster_info.stderr) or
    (item not in cluster_info_properties)

- name: Create resources for the cluster
  shell: >
    {{ lookup('template', 'templates/pcs_resource_command.j2') }}
  loop: "{{ cluster.resources }}"
  when:
    - (cluster.resources | length) > 0
  register: add_resources
  changed_when: "add_resources.rc == 0"
  failed_when: "add_resources.rc != 0 and 'already exists' not in add_resources.stderr"

- name: Create group for resources
  shell: >
    {{ lookup('template', 'templates/pcs_groups_command.j2') }}
  loop: "{{ cluster.groups }}"
  when:
    - (cluster.groups | length) > 0
    - cluster_info_groups is not defined
  register: add_groups
  changed_when: "add_groups.rc == 0"
  failed_when: "add_groups.rc != 0 and 'already exists' not in add_groups.stderr"

- name: Update group for resources
  shell: >
    {{ lookup('template', 'templates/pcs_groups_command.j2') }}
  loop: "{{ cluster.groups }}"
  when:
    - (cluster.groups | length) > 0
    - cluster_info_group_members is defined
    - (item.resource_members | difference(cluster_info_group_members) | length > 0)
  register: add_groups
  changed_when: "add_groups.rc == 0"
  failed_when: "add_groups.rc != 0 and 'already exists' not in add_groups.stderr"

- name: Create constraints for resources
  shell: >
    {{ lookup('template', 'templates/pcs_constraints_command.j2') }}
  loop: "{{ cluster.constraints }}"
  when: (cluster.constraints | length) > 0
  register: add_constraints
  changed_when: "add_constraints.rc == 0"
  failed_when: "add_constraints.rc != 0 and 'duplicate constraint' not in add_constraints.stderr"
