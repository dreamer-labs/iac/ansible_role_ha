---
- name: Gather cluster information
  command: |
    pcs cluster cib
  register: cluster_info
  changed_when: false
  failed_when: false

- name: Extract cluster name
  community.general.xml:
    xmlstring: "{{ cluster_info.stdout }}"
    xpath: /cib/configuration/crm_config/cluster_property_set/nvpair[@id='cib-bootstrap-options-cluster-name']
    content: attribute
  register: cluster_info_name_xml
  when: "'unable to get cib' not in cluster_info.stderr"

- name: Extract cluster properties
  community.general.xml:
    xmlstring: "{{ cluster_info.stdout }}"
    xpath: /cib/configuration/crm_config/cluster_property_set/nvpair
    content: attribute
  register: cluster_info_properties_xml
  when: "'unable to get cib' not in cluster_info.stderr"

- name: Extract cluster resources
  community.general.xml:
    xmlstring: "{{ cluster_info.stdout }}"
    xpath: /cib/configuration/resources//primitive
    content: attribute
  register: cluster_info_resources_xml
  failed_when: false
  when: "'unable to get cib' not in cluster_info.stderr"

- name: Extract cluster resource properties
  community.general.xml:
    xmlstring: "{{ cluster_info.stdout }}"
    xpath: /cib/configuration/resources//primitive//nvpair
    content: attribute
  register: cluster_info_resources_properties_xml
  failed_when: false
  when: "'unable to get cib' not in cluster_info.stderr"

- name: Extract cluster groups
  community.general.xml:
    xmlstring: "{{ cluster_info.stdout }}"
    xpath: /cib/configuration/resources/group
    content: attribute
  register: cluster_info_groups_xml
  failed_when: false
  when: "'unable to get cib' not in cluster_info.stderr"

- name: Extract cluster group members
  community.general.xml:
    xmlstring: "{{ cluster_info.stdout }}"
    xpath: /cib/configuration/resources/group/primitive
    content: attribute
  register: cluster_info_group_members_xml
  failed_when: false
  when: "'unable to get cib' not in cluster_info.stderr"

- name: Set cluster resource names
  set_fact:
    cluster_resource_names: "{{ cluster_info_resources_xml.matches | map(attribute='primitive') | map(attribute='id') }}"
  when:
    - "'unable to get cib' not in cluster_info.stderr"
    - cluster_info_resources_xml.matches is defined

- name: Set temp cluster resource property fact
  set_fact:
    tmp_resource_property_fact:
      "{'{{ item.name }}': '{{ item.value }}'}"
  with_items:
    - "{{ cluster_info_resources_properties_xml.matches | map(attribute='nvpair') }}"
  register: tmp_resource_property_fact_result
  when:
    - "'unable to get cib' not in cluster_info.stderr"
    - cluster_info_resources_properties_xml.matches is defined

- name: Set cluster properties fact
  set_fact:
    cluster_resource_properties: "{{ tmp_resource_property_fact_result.results | map(attribute='ansible_facts.tmp_resource_property_fact') | list }}"
  when:
    - "'unable to get cib' not in cluster_info.stderr"
    - tmp_resource_property_fact_result.results is defined

- name: Set cluster info properties
  set_fact:
    cluster_info_properties: "{{ cluster_info_properties_xml.matches | map(attribute='nvpair') }}"
  when:
    - "'unable to get cib' not in cluster_info.stderr"
    - cluster_info_properties_xml.matches is defined

- name: Set cluster info resources
  set_fact:
    cluster_info_resources: "{{ cluster_info_resources_xml.matches }}"
  when:
    - "'unable to get cib' not in cluster_info.stderr"
    - cluster_info_resources_xml.matches is defined

- name: Set cluster info groups
  set_fact:
    cluster_info_groups: "{{ cluster_info_groups_xml.matches }}"
  when:
    - "'unable to get cib' not in cluster_info.stderr"
    - cluster_info_groups_xml.matches is defined

- name: Set cluster info group members
  set_fact:
    cluster_info_group_members: "{{ cluster_info_group_members_xml.matches | map(attribute='primitive') | map(attribute='id') }}"
  when:
    - "'unable to get cib' not in cluster_info.stderr"
    - cluster_info_group_members_xml.matches is defined

- name: Set cluster info name
  set_fact:
    cluster_info_name: "{{ cluster_info_name_xml.matches[0].nvpair.value }}"
  when:
    - "'unable to get cib' not in cluster_info.stderr"
    - cluster_info_name_xml.matches is defined

- name: Set cluster_info_properties temp fact
  set_fact:
    tmp_property_fact:
      "{'name': '{{ item.name }}', 'value': '{{ item.value }}'}"
  with_items: "{{ cluster_info_properties }}"
  register: tmp_property_fact_result
  when: "'unable to get cib' not in cluster_info.stderr"

- name: Set cluster info properties fact
  set_fact:
    cluster_info_properties: "{{ tmp_property_fact_result.results | map(attribute='ansible_facts.tmp_property_fact') | list }}"
  when: "'unable to get cib' not in cluster_info.stderr"
