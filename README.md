Role Name
=========

This role will setup an HA cluster with at least three nodes

Requirements
------------

At least a minimum of three nodes

Role Variables
--------------

Example:

```
ha_cluster_password: changeme
ha_cluster_password_salt: changeme
ha_cluster_nodes: ['node1', 'node2', node3']
ha_clusters:
  - name: My Cluster
    nodes:
    properties:
      - name: "stonith-enabled"
        value: "false"
      - name: "no-quorum-policy"
        value: "ignore"
    resources:
      - name: blah
        type: ocf:heartbeat:IPaddr2
        options:
          nic: eth0
          ip: 127.0.0.2
          cidr_netmask: 24
        monitor_interval: 20
      - name: zabbix-agent
        type: systemd:httpd
        monitor_interval: 60
    groups:
      - name: zabbix_httpd_grp
        resource_members:
          - blah
          - zabbix-agent
    constraints:
      - name: colocation
        options:
          type: add
          resources:
            - zabbix_httpd_grp
            - blah
          score: INFINITY
      - name: order
        options:
          - resource: blah
            action: start
          - resource: zabbix_httpd_grp
            action: start
```

Dependencies
------------

None

Example Playbook
----------------

```
- hosts: zabbix
  roles:
    - { role: ansible_role_ha }
```

License
-------

MIT

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
